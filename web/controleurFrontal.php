<?php

////////////////////
// Initialisation //
////////////////////
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../vendor/autoload.php';


/////////////
// Routage //
/////////////
$requete = Request::createFromGlobals();

$reponse = TheFeed\Controleur\RouteurURL::traiterRequete($requete);
$reponse->send();

