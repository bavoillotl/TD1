<?php

use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;

/** @var UrlGenerator $generateurUrl */
$generateurUrl = Conteneur::recupererService("generateurUrl");
/** @var UrlHelper $assistantUrl */
$assistantUrl = Conteneur::recupererService("assistantUrl");

/**
 * @var string $pagetitle
 * @var string $cheminVueBody
 * @var String[][] $messagesFlash
 */

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?= $pagetitle ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?= $assistantUrl->getAbsoluteUrl("../ressources/css/styles.css") ?>"
</head>

<body>
    <header>
        <div id="titre" class="center">
            <a href="<?=$generateurUrl->generate("afficherListe"); ?>"><span>The Feed</span></a>
            <nav>
               <!-- <a href="controleurFrontal.php?controleur=publication&action=afficherListe">Accueil</a> -->
                <a href="<?=$generateurUrl->generate("afficherListe"); ?>">Accueil</a>
                <?php
                if (!ConnexionUtilisateur::estConnecte()) {
                ?>
                    <a href="<?=$generateurUrl->generate("inscription"); ?>">Inscription</a>
                    <a href="<?=$generateurUrl->generate("afficherFormulaireConnexion"); ?>">Connexion</a>
                <?php
                } else {
                    $idUtilisateurURL = rawurlencode(ConnexionUtilisateur::getIdUtilisateurConnecte());
                ?>
                    <a href="<?=$generateurUrl->generate("afficherPublication", ["idUtilisateur" => $idUtilisateurURL]); ?>">Ma
                        page</a>
                    <a href="<?=$generateurUrl->generate("deconnecter"); ?>">Déconnexion</a>
                <?php } ?>
            </nav>
        </div>
    </header>
    <div id="flashes-container">
        <?php
        foreach (["success", "error"] as $type) {
            foreach ($messagesFlash[$type] as $messageFlash) {
        ?>
                <span class="flashes flashes-<?= $type ?>"><?= $messageFlash ?></span>
        <?php
            }
        }
        ?>
    </div>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</body>

</html>