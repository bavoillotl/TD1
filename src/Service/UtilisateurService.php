<?php

namespace TheFeed\Service;

use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{
    public function __construct(private UtilisateurRepositoryInterface $utilisateurRepository, private FileMovingServiceInterface $fileMovingService, private string $dossierPhotoDeProfil)
    {}
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil) : void {
        //TO-DO
        //Verifier que les attributs ne sont pas null
        if (
            !is_null($login)
//            && isset($_POST['mot-de-passe']) && isset($_POST['email'])
//            && isset($_FILES['nom-photo-de-profil'])
        ) {
//            $login = $_POST['login'];
//            $motDePasse = $_POST['mot-de-passe'];
//            $adresseMail = $_POST['email'];
//            $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'];

            //Verifier la taille du login
            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");
            }

            //Verifier la validité du mot de passe
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException("Mot de passe invalide!");
            }

            //Verifier le format de l'adresse mail
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException("L'adresse mail est incorrecte!");
            }

            //Verifier que l'utilisateur n'existe pas déjà
            $utilisateurRepository = $this->utilisateurRepository;
            $utilisateur = $utilisateurRepository->recupererParLogin($login);
            if ($utilisateur != null) {
                throw new ServiceException("Ce login est déjà pris!");
            }

            //Verifier que l'adresse mail n'est pas prise
            $utilisateur = $utilisateurRepository->recupererParEmail($email);
            if ($utilisateur != null) {
                throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");
            }

            //Verifier extension photo de profil
            $explosion = explode('.', $donneesPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException("La photo de profil n'est pas au bon format!");
            }

            //Enregistrer la photo de profil
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $donneesPhotoDeProfil['tmp_name'];
            $to = $this->dossierPhotoDeProfil . "/".$pictureName;
            var_dump($to);
            $this->fileMovingService->moveFile($from, $to);

            //Chiffrer le mot de passe
            //Enregistrer l'utilisateur...
            $mdpHache = MotDePasse::hacher($motDePasse);

            $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
            $utilisateurRepository->ajouter($utilisateur);
        }else {
            throw new ServiceException("Login, nom, prenom ou mot de passe manquant.");
        }
    }

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true) : ?Utilisateur {
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);
     if(!$autoriserNull && $utilisateur == null ) {
            throw new ServiceException("l’utilisateur n’existe pas!");
        }
     return $utilisateur;
 }

    public function connecterUtilisateur(string $login, string $mdp) : void {
        if (!(isset($login) && isset($mdp))) {
            throw new ServiceException("Login ou mot de passe manquant.");
        }
        $utilisateur = $this->utilisateurRepository->recupererParLogin($login);

        if ($utilisateur == null) {
            throw new ServiceException("Login inconnu.");
        }

        if (!MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect.");
        }

        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }

    public function deconnecterUtilisateur() : void
    {
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("Utilisateur non connecté.");
        }
        ConnexionUtilisateur::deconnecter();
    }
}