<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Utilisateur;

interface UtilisateurServiceInterface
{
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil): void;

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur;

    public function connecterUtilisateur(string $login, string $mdp): void;

    public function deconnecterUtilisateur(): void;
}