<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurUtilisateur extends ControleurGenerique
{
    public function __construct(ContainerInterface $container, private UtilisateurServiceInterface $utilisateurService,
                                private PublicationServiceInterface $publicationService)
    {
        parent::__construct($container);
    }
    public function afficherErreur($messageErreur = "", $controleur = ""): Response
    {
        return parent::afficherErreur($messageErreur, "utilisateur");
    }

    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublication', methods:["GET"])]
    public function afficherPublications($idUtilisateur): Response
    {
        try {
            $utilisateur = $this->utilisateurService->recupererUtilisateurParId($idUtilisateur, false);
            $loginHTML = htmlspecialchars($utilisateur->getLogin());
            $publications = $this->publicationService->recupererPublicationsUtilisateur($idUtilisateur);
            return $this->afficherTwig("publication/page_perso.html.twig", ["publications" => $publications, "pagetitle" => "Page perso de $loginHTML"]);
        } catch (ServiceException $e) {
        MessageFlash::ajouter("erreur", $e);
    }

        return ControleurPublication::rediriger('afficherListe');
    }

    #[Route(path: '/inscription', name:'inscription', methods:["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig("utilisateur/inscription.html.twig",
            [
            "method" => Configuration::getDebug() ? "get" : "post",
            ]);
    }

    #[Route(path: '/inscription', name:'creerDepuisFormulaireInscription', methods:["POST"])]
    public function creerDepuisFormulaire(): Response {
        //Recupérer les différentes variables (login, mot de passe, adresse mail, données photo de profil...)
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;
        $adresseMail = $_POST['email'] ?? null;
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;
        try {
            $this->utilisateurService->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
        }
        catch(ServiceException $e) {
            //Ajouter message flash d'erreur
            MessageFlash::ajouter("erreur", $e);
            //Rediriger sur le formulaire de création
            return $this->rediriger("inscription");
        }
        //Ajouter un message flash de succès (L'utilisateur a bien été créé !)
        MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");

        //Rediriger sur la page d'accueil (route afficherListe)
        return $this->rediriger("afficherListe");

    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
       return $this->afficherTwig("utilisateur/connexion.html.twig");

    }
    #[Route(path: '/connexion', name:'connecter', methods:["POST"])]
    public function connecter(): Response
    {
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;

        try {
            $this->utilisateurService->connecterUtilisateur($login, $motDePasse);
        }catch (ServiceException $e){
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherFormulaireConnexion");
        }
        MessageFlash::ajouter("success", "Connexion effectuée.");
        return $this->rediriger("afficherListe");
    }
    #[Route(path: '/deconnexion', name:'deconnecter', methods:["GET"])]
    public function deconnecter(): Response
    {
        try{
            $this->utilisateurService->deconnecterUtilisateur();
        }catch (ServiceException $e){
            MessageFlash::ajouter("error", $e);
            return  $this->rediriger( "afficherListe");
        }

        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
      return  $this->rediriger( "afficherListe");
    }
}
