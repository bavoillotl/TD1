<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;

class ControleurPublication extends ControleurGenerique
{
    public function __construct(ContainerInterface $container, private PublicationServiceInterface $publicationService)
    {
        parent::__construct($container);
    }

    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    #[Route(path: '/', name:'web')]
    public function afficherListe(): Response
    {
        $publications = $this->publicationService->recupererPublications();
         return $this->afficherTwig("publication/feed.html.twig", ["publications" => $publications]);
    }

    #[Route(path: '/publications', name:'creerDepuisFormulaire', methods:["POST"])]
        public function creerDepuisFormulaire() : Response
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            $this->publicationService->creerPublication($idUtilisateurConnecte,$message);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("erreur", $e);
        }

        return $this->rediriger('afficherListe');
    }


}