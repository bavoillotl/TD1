<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\ConnexionBaseDeDonneesInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;

class UtilisateurRepositoryTest extends TestCase
{
    private static UtilisateurRepositoryInterface  $utilisateurRepository;

    private static ConnexionBaseDeDonneesInterface $connexionBaseDeDonnees;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$connexionBaseDeDonnees = new ConnexionBaseDeDonnees(new ConfigurationBDDTestUnitaire());
        self::$utilisateurRepository = new UtilisateurRepository(self::$connexionBaseDeDonnees);
    }

    protected function setUp(): void
    {
        parent::setUp();
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (1, 'test', 'test', 'test@example.com', 'test.png')");
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (2, 'test2', 'test2', 'test2@example.com', 'test2.png')");
    }

    public function testSimpleNombreUtilisateurs() {
        $this->assertCount(2, self::$utilisateurRepository->recuperer());
    }

    public function testNombreUtilisateur(){
        $utilisateur = self::$utilisateurRepository->recupererParClePrimaire(1);
        self::assertEquals("test", $utilisateur->getLogin());
        self::assertEquals("test", $utilisateur->getMdpHache());
        self::assertEquals("test@example.com", $utilisateur->getEmail());
    }

    public function testAjouterUtilisateur(){
        self::$utilisateurRepository->ajouter(Utilisateur::create("3", "test3", "test3@exemple.com", "testPhoto"));
        assertCount(3, self::$utilisateurRepository->recuperer());
    }
    protected function tearDown(): void
    {
        parent::tearDown();
        self::$connexionBaseDeDonnees->getPdo()->query("DELETE FROM utilisateurs");
    }

}