<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;

class PublicationServiceTest extends TestCase {
    private $service;
    private $publicationRepositoryMock;
    private $utilisateurRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepositoryInterface::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->service = new PublicationService($this->publicationRepositoryMock, $this->utilisateurRepositoryMock);
    }

    public function testCreerPublicationUtilisateurInexistant(){
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(null);
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");
        $this->expectException(ServiceException::class);
        $this->service->creerPublication(-1, "coucou");
    }

    public function testCreerPublicationVide(){
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());
        $this->expectExceptionMessage("Le message ne peut pas être vide!");
        $this->expectException(ServiceException::class);
        $this->service->creerPublication(4, "");
    }

    public function testCreerPublicationTropGrande(){
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->expectException(ServiceException::class);
        $this->service->creerPublication(4, str_repeat("lele", 100));
    }

    public function testNombrePublications(){
        //Fausses publications, vides
        $fakePublications = [new Publication(), new Publication()];
        //On configure notre faux repository pour qu'il renvoie nos publications définies ci-dessus
        $this->publicationRepositoryMock->method("recuperer")->willReturn($fakePublications);
        //Test
        $this->assertCount(2, $this->service->recupererPublications());
    }

    public function testNombrePublicationsUtilisateur(){
        $fakePublications = [new Publication()];
        $this->publicationRepositoryMock->method("recupererParAuteur")->willReturn($fakePublications);
        $nb = sizeof($this->service->recupererPublicationsUtilisateur(3));
        self::assertEquals(1, $nb);
    }

    public function testCreerPublicationValide(){
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(Utilisateur::create("lele", "leleinfo", "lele@kve.vco","getgqh"));
        $this->publicationRepositoryMock->method("ajouter")->willReturnCallback(function ($pub){
            $this->publicationRepositoryMock->method("recuperer")->willReturn([$pub]);
        });
        $this->service->creerPublication(1, "test");
        self::assertCount(1, $this->service->recupererPublications());
    }

    public function testNombrePublicationsUtilisateurInexistant(){
        $this->publicationRepositoryMock->method("recupererParAuteur")->willReturn([]);
        $nb = sizeof($this->service->recupererPublicationsUtilisateur(-1));
        self::assertEquals(0, $nb);
    }
}