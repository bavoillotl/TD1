<?php

namespace TheFeed\Test;

use TheFeed\Service\FileMovingServiceInterface;

class TestFileMovingService implements FileMovingServiceInterface
{
    private static string $ASSETS_FOLDER = __DIR__."/assets/";

    public function moveFile($fileName, $pathDestination)
    {
        copy(self::$ASSETS_FOLDER.$fileName, $pathDestination);
    }

}